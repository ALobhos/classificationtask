import pandas as pd
import preprocessor
import clasificators


def getData():
    try:
        rawData = pd.read_csv("./dataset.csv")
    except IOError:
        print("No se ha encontrado el archivo de datos, intentando desde web")

        try:
            url = "https://gitlab.com/ALobhos/classificationtask/-/raw/main/dataset.csv"
            rawData = pd.read_csv(url, header=None)
        except:
            print("No se puede conectar con el servidor web. Compruebe su "
                  "conexión a internet e intente denuevo")
            exit(1)
    

    return rawData

def generateModels(data):

    clasificators.kNN(data)
    clasificators.decissionTree(data)
    clasificators.naiveBayes(data)
    clasificators.randomForest(data)


def main():

    rawData = getData()
    cleanData = preprocessor.dataCleaner(rawData)
    generateModels(cleanData)
    

if __name__ == "__main__":
    main()
